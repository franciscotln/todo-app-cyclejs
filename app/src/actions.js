export const initialState = { items: [], input: { value: '' } };

export const actions = {
  addItem: (itemText) =>
    (state) => Object.assign({}, state, { items: state.items.concat({ text: itemText, isEditing: false }) })
  ,
  removeItem: (itemText) =>
    (state) => Object.assign({}, state, { items: state.items.filter(item => item.text !== itemText) })
  ,
  updateInput: (value) =>
    (state) => Object.assign({}, state, { input: { value } })
  ,
  clearInput: () =>
    (state) => Object.assign({}, state, { input: { value: '' } })
  ,
  editItem: (itemText) =>
    (state) => Object.assign({}, state, {
      items: state.items.map(item => {
        if(item.isEditing) {
          item.isEditing = false;
        }
        if(item.text === itemText) {
          item.isEditing = true;
        }
        return item;
      })
    })
  ,
  saveItem: (newItemText) =>
    (state) => Object.assign({}, state, {
      items: state.items.map(item => {
        if (item.isEditing) {
          item.text = newItemText;
          item.isEditing = false;
        }
        return item;
      })
    })
  ,
  cancelEdition: () =>
    (state) => Object.assign({}, state, {
      items: state.items.map(item => {
        if (item.isEditing) {
          item.isEditing = false;
        }
        return item;
      })
    })
};

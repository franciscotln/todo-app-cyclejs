export function intent(DOMSources) {

  return {
    updateInput: DOMSources.select('.add-todo').events('input').map(event => event.target.value)
  ,
    addTodo: DOMSources.select('.add-todo').events('change').map(event => event.target.value)
      .filter(value => value.trim().length)
  ,
    removeTodo: DOMSources.select('.delete-button').events('click')
      .map((event) => Array.from(event.target.parentNode.children)
        .find((child) => child.className === 'todo-detail')
        .innerText.trim()
      )
  ,
    editTodo: DOMSources.select('.edit-button').events('click')
      .map((event) => Array.from(event.target.parentNode.children)
        .find((child) => child.className === 'todo-detail')
        .innerText.trim()
      )
  ,
    saveTodo: DOMSources.select('.edit-todo').events('change').map(event => event.target.value)
      .filter(value => value.trim().length),

    cancelEdition: DOMSources.select('.cancel-button').events('click')
  };
}

'use strict';

import { run } from '@cycle/rxjs-run';
import { makeDOMDriver } from '@cycle/dom';
import { intent } from './intent';
import { model } from './model';
import { view } from './view';

function main(sources) {
  return {
    DOM: view(model(intent(sources.DOM)))
  };
}

const drivers = { DOM: makeDOMDriver('body') };

const disposeApp = run(main, drivers);

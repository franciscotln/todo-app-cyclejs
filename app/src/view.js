import { div, button, input, span, p, h3 } from '@cycle/dom';
import { initialState } from './actions';

function renderIfEditing(item) {
  return item.isEditing ?
    [
      input('.edit-todo', { props: { type: 'text', value: item.text } }),
      button('.cancel-button', { props: { type: 'button'} }, 'Cancel')
    ]
    :
    [button('.edit-button', { props: { type: 'button' } }, 'Edit')];
}

function Todo(item) {
  return [
    span('.todo-detail', item.text),
    ...renderIfEditing(item),
    button('.delete-button', { props: { type: 'button' } }, 'Remove')
  ];
}

export function view(state$) {
  return state$.startWith(initialState).map((state) =>
    div('.app-container', [
      div([
        h3('TODO App in Cycle.js'),
        input('.add-todo', { props: { type: 'text', placeholder: 'Enter a todo', value: state.input.value } }),
      ]),
      div('.todos-container', state.items.map(item =>
        p('.todo', Todo(item))
      ))
    ])
  );
}

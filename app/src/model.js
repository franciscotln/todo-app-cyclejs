import { actions, initialState } from './actions';
import { Observable } from 'rxjs';

export function model(intents) {

  const addItemActions$ = intents.addTodo.map(actions.addItem);

  const clearInput$ = intents.addTodo.map(actions.clearInput);

  const updateInput$ = intents.updateInput.map(actions.updateInput);

  const removeItemActions$ = intents.removeTodo.map(actions.removeItem);

  const editItemActions$ = intents.editTodo.map(actions.editItem);

  const saveItemActions$ = intents.saveTodo.map(actions.saveItem);

  const cancelEditionActions$ = intents.cancelEdition.map(actions.cancelEdition)

  const actions$ = Observable.merge(addItemActions$, updateInput$, clearInput$,
    removeItemActions$, editItemActions$, saveItemActions$, cancelEditionActions$);

  const state$ = actions$.scan((state, action) => action(state), initialState);

  return state$;
}

# README #

### Small Todo App written in Cycle.js ###

* Functionalities: add, edit, delete todos from a list. (no styles, no immutableJS ;-) )
* v0.0.1

### Installation ###

```
Requirements:
npm version: 3.10.8
node version: 4.4.4
nvm version: 0.31.0

npm i
npm start
```
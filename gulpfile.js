'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const replace = require('gulp-replace');
const babelify = require('babelify');
const browserify = require('browserify');
const vinylSourceStream = require('vinyl-source-stream');
const vinylBuffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const merge = require('merge-stream');
const args = require('yargs').argv;
const rename = require('gulp-rename');
const inject = require('gulp-inject');
const runSequence = require('run-sequence');

const ENV = ['test', 'production', 'staging'].indexOf(args.env) > -1 ? args.env : 'development';

const paths = {
  build: {
    src: {},
    dest: {}
  },
  dist: {
    src: {},
    dest: {}
  }
};

paths.build.src = {
  scripts: [
    'app/src/app.js',
    'app/src/**/*.js',
  ],
  styles: [
    'app/src/**/*.css'
  ],
  templates: {
    index: [
      'app/src/index.html'
    ],
    partials: [
      'app/src/modules/**/*.html',
    ]
  },
  images: [],
  vendor: [],
  fonts: []
};

paths.build.dest = {
  app: 'app/build/scripts',
  styles: 'app/build/styles',
  templates: {
    index: 'app/build',
    partials: 'app/build/templates'
  },
  images: 'app/build/images',
  vendor: 'app/build/vendor',
  fonts: 'app/build/fonts'
};

paths.dist.src = {
  styles: [
    'app/build/styles/*.css'
  ],
  scripts: [
    'app/build/scripts/*.js'
  ],
  templates: {
    index: 'app/src/index.html',
    partials: 'app/build/templates'
  },
  fonts: 'app/build/fonts/*',
  images: 'app/build/**/*.{jpg,png,svg,gif}'
};

paths.dist.dest = {
  styles: 'www/',
  scripts: 'www/',
  templates: {
    index: 'www/',
    templates: 'www/templates'
  },
  fonts: 'www/fonts/',
  images: 'www/'
};

gulp.task('build:scripts', () =>
  browserify({
    entries: paths.build.src.scripts[0],
    debug: true
  })
    .transform('babelify', { presets: ['es2015'] })
    .bundle()
    .pipe(vinylSourceStream('app.js'))
    .pipe(vinylBuffer())
    .pipe(gulp.dest(paths.build.dest.app))
);

gulp.task('build:styles', () =>
  gulp.src(paths.build.src.styles)
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(paths.build.dest.styles))
);

gulp.task('build:templates', () => {
  const ngTemplates = gulp.src(paths.build.src.templates.partials)
    .pipe(gulp.dest(paths.build.dest.templates.partials));

  const indexPage = gulp.src(paths.build.src.templates.index)
    .pipe(inject(gulp.src([
      'app/build/styles/styles.css',
      'app/build/vendor/*.js',
      'app/build/scripts/*.js'
    ], { read: false })))
    .pipe(replace('app/build/', ''))
    .pipe(gulp.dest(paths.build.dest.templates.index));

  return merge(ngTemplates, indexPage);
});

gulp.task('build:images', () =>
  gulp.src(paths.build.src.images)
    .pipe(gulp.dest(paths.build.dest.images))
);

gulp.task('build:vendor', () =>
  gulp.src(paths.build.src.vendor)
    .pipe(gulp.dest(paths.build.dest.vendor))
);

gulp.task('build:fonts', () =>
  gulp.src(paths.build.src.fonts)
    .pipe(gulp.dest(paths.build.dest.fonts))
);

gulp.task('watch', () => {
  gulp.watch(paths.build.src.configs, ['build:config']);
  gulp.watch(paths.build.src.scripts, ['build:scripts']);
  gulp.watch(paths.build.src.styles, ['build:styles']);
  gulp.watch([paths.build.src.templates.index, paths.build.src.templates.partials], ['build:templates']);
  gulp.watch(paths.build.src.images ['build:images']);
});

gulp.task('build', (callback) => {
  const sequenceArray = [
    'build:scripts',
    'build:styles',
    'build:images',
    'build:vendor',
    'build:fonts'
  ];
  runSequence(sequenceArray, 'build:templates', callback);
});

gulp.task('dist', () => {
  const styles = gulp.src(paths.dist.src.styles)
    .pipe(replace('../fonts/', 'fonts/'))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(paths.dist.dest.styles));

  const scripts = gulp.src(paths.dist.src.scripts)
    .pipe(uglify())
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(paths.dist.dest.scripts));

  const index = gulp.src(paths.dist.src.templates.index)
    .pipe(replace('<!-- inject:css-->', "<link rel='stylesheet' href='styles.css' />"))
    .pipe(replace('<!-- inject:js-->', "<script src='scripts.js'></script>"))
    .pipe(gulp.dest(paths.dist.dest.templates.index));

  const fonts = gulp.src(paths.dist.src.fonts)
    .pipe(gulp.dest(paths.dist.dest.fonts));

  const images = gulp.src(paths.dist.src.images)
    .pipe(gulp.dest(paths.dist.dest.images));

  return merge(styles, scripts, index, fonts, images);
});

gulp.task('default', ['build', 'watch']);
